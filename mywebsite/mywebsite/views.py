from django.shortcuts import render_to_response
from django.template import RequestContext

def home(request):
	ctx=RequestContext(request)
	temp_name="index.html"
	response = render_to_response(temp_name, {},ctx)
	return response

def about(request):
	ctx=RequestContext(request)
	temp_name="about.html"
	response = render_to_response(temp_name, {},ctx)
	return response
